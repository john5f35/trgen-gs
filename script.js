/**
 * Teaching report generation script
 *
 * MIT License
 * 
 * Copyright (c) 2018 John Zhang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * This script generates report for every student according to
 * a predefined template in Google Spreadsheet.
 * 
 * Set up:
 * - Open Tools -> Script editor;
 * - Copy paste the code.
 * - Create two sheets, one for students record, the other for template 
 *    (e.g. "Input", "Commentbank-Transposed").
 * 
 * Usage:
 * - In the template sheet, define the names of "sections" of report in left-most column;
 *    and define the names of "grades" of each section in the top row.
 *    Then in each cell, define the template string. So it looks something like this:
 *    | Section       | A | B | C | D | E |
 *    |---------------|---|---|---|---|---|
 *    | Opening       |   |   |   |   |   |
 *    |---------------|---|---|---|---|---|
 *    | Assignment 1  |   |   |   |   |   |
 *    |---------------|---|---|---|---|---|
 *    | Assignment 2  |   |   |   |   |   |
 *    |---------------|---|---|---|---|---|
 * 
 * - Each template string can mark certain words for automatic replacements.
 *    - #NAME -> the name of the student
 *    - #HE   -> he/she depending on gender
 *    - #HIS  -> his/her depending on gender
 *    - #HIM  -> him/her depending on gender
 * - In the students sheet, the header will be automatically generated according to the template.
 *    A "Generated Report" column will be added to the end to contain the report for the student.
 * - Input each student's details, and the report will be generated.
 *    If one grade is wrong (typo), or the corresponding template string is empty,
 *    the background of the cell will be red, and no report will be generated.
 * 
 * NOTE:
 * - Everytime an edit happens (in either sheet), the script will be run for all students.
 * - Grades doesn't have to be A~E; it can be anything as long as the value for the student info matches up.
 */

var sheet_name_students = "Input";                    // MODIFY FOR NEW SPREADSHEET
var sheet_name_template = "Commentbank-Transposed";   // MODIFY FOR NEW SPREADSHEET

// template sheet
var section_start_row = 2;

// input sheet
var name_start_row = 2;

function generate_report() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheet_template = spreadsheet.getSheetByName(sheet_name_template);  
  var sheet_students = spreadsheet.getSheetByName(sheet_name_students);

  var [template, sections, grades] = load_template(sheet_template);
  
  // set header
  var student_attrs = ["Name", "Gender"].concat(sections);
  var header = student_attrs.concat(["Generated Report"]);
  sheet_students.getRange(1, 1, 1, header.length).setValues([header]);
  
  var row = 2;
  while (cell_at(sheet_students, row, 1)) {  // if student name is not empty
    var student = row2obj(sheet_students, row, student_attrs);
    var report = "";
    if (validate_student_info(sheet_students, student, row, template, sections))
      report = apply_template(student, template, sections);
    sheet_students.getRange(row, header.length).setValue(report);

    row += 1;
  }
}

function validate_student_info(sheet, stu, row, tmp, secs, header) {
  return secs.map(function (sec) {
    if (stu[sec] != "" && tmp[sec] == "")
      return false;
    if (stu[sec] != "" && !(stu[sec] in tmp[sec]))
      return false;
    return true;
  }).reduce(function (acc, suc, idx, arr) {
    if (suc)
      sheet.getRange(row, idx + 3).setBackgroundRGB(255, 255, 255);
    else
      sheet.getRange(row, idx + 3).setBackgroundRGB(223, 80, 80);
    return acc & suc;
  }, true);
}

function apply_template(student, template, sections) {
  var pronoun = {
    "#HE": {"M": "he", "F": "she"},
    "#HIM": {"M": "him", "F": "her"},
    "#HIS": {"M": "his", "F": "her"}
  };

  return sections.map(function (sec) {
    if (student[sec] != "" && student[sec] in template[sec]) {
      var tmpstr = template[sec][student[sec]];
      if (tmpstr) {
        // apply template transformation
        tmpstr = tmpstr.replace(new RegExp("#NAME", "gi"), student["Name"]);
        for (var p in pronoun) {
          tmpstr = tmpstr.replace(new RegExp(p, "gi"), pronoun[p][student["Gender"]]);
        }
        
        // trim white spaces at beginning and end
        tmpstr = tmpstr.replace(/^\s+|\s+$/g, '');

        // fix upper case at beginning of string.
        tmpstr = tmpstr[0].toUpperCase() + tmpstr.slice(1);
        // fix upper case at start of sentences
        var idx = 0;
        while((idx = tmpstr.indexOf(".", idx + 1)) != -1) {
          if (idx + 2 < tmpstr.length)
            tmpstr = tmpstr.slice(0, idx + 2) + tmpstr[idx + 2].toUpperCase() + tmpstr.slice(idx + 3);
        }
      }
      return tmpstr;
    }
  }).filter(function (s) { return s != ""; }).join(" ");
}

function row2obj(sheet, row, names) {
  var numcols = names.length;
  var data = sheet.getRange(row, 1, 1, numcols).getValues()[0];
  var obj = {};
  for (var col = 0; col < numcols; col += 1) {
    obj[names[col]] = data[col];
  }
  return obj;
}

function cell_at(sheet, row, col) {
  return sheet.getRange(row, col).getValue();
}

function load_template(sheet) {
  var template = {};
  
  // work out the number of "grades"
  var grades = [];
  var i = 0;
  var grade;
  while((grade = cell_at(sheet, 1, i + 2))) {
    grades.push(grade);
    i += 1;
  }
  
  // include the first "Section" field which will be removed later.
  var attr_names = sheet.getRange(1, 1, 1, 1 + grades.length).getValues()[0];

  var row = section_start_row;
  var sections = [];
  while (cell_at(sheet, row, 1)) {
    var section = row2obj(sheet, row, attr_names);
    template[section["Section"]] = section;
    sections.push(section["Section"]);
    delete section["Section"];
    row += 1;
  }
  return [template, sections, grades];
}